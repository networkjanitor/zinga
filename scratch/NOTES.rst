Notes
-----

data files as yml files

config files (bundler) as yml files
 - define path of data yml files and in which order and pages they should be included
 - maybe offer import paramterization/templating, e.g.

   .. code:
      ---
      - from: tripletrouble/general/rules.yml
        with:
          rally_waypoint: "[&AbCdEf]"
          wurm: "Cobalt"
      

Disadvantages
-------------
No support for nested configurations (as with .py bundler files)

Advantages
----------

Wayyy cleaner implementation without hidden and convoluted functionality
variable templating system not used anyway (except for OC links)
