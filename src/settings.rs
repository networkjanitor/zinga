use std::path::PathBuf;
use config::{ConfigError, Config, File, Environment};

#[derive(Debug, Deserialize)]
pub struct Directories {
    pub sources: String,
    pub bundlers: String,
}

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub directories: Directories,
}

impl Settings {
    pub fn new<P: Into<PathBuf>>(config_path: P) -> Result<Self, ConfigError> {
        let mut s = Config::new();

        // Start off by merging in the "default" configuration file
        s.merge(File::with_name(config_path.into().to_str().unwrap()))?;

        // Add in settings from the environment (with a prefix of APP)
        // Eg.. `APP_DEBUG=1 ./target/app` would set the `debug` key
        s.merge(Environment::with_prefix("ZINGA"))?;

        // Now that we're done, let's access our configuration
        //println!("sources: {:?}", s.get::<String>("directories.sources"));

        // You can deserialize (and thus freeze) the entire configuration as
        s.try_into()
    }
}
