#[macro_use]
extern crate serde_derive;
extern crate config;
extern crate serde_yaml;
extern crate structopt;
extern crate walkdir;

mod settings;

use std::collections::HashMap;
use std::path::PathBuf;
use structopt::StructOpt;
use walkdir::{DirEntry, WalkDir};

#[derive(StructOpt, Debug)]
#[structopt(name = "zinga")]
pub struct Opt {
    /// Path to the zinga.toml configuration file, containing all the neccessary directories
    /// configurations.
    #[structopt(
        short = "c",
        long = "config-path",
        default_value = "zinga.toml",
        parse(from_os_str)
    )]
    config_path: PathBuf,
}

#[derive(Debug, Serialize, Deserialize)]
struct Source {
    #[serde(skip)]
    rela_path: String,
    messages: Vec<Message>,
}

#[derive(Debug, Serialize, Deserialize)]
struct Bundle {
    title: String,
    tabs: Vec<Tab>,
}

#[derive(Debug, Serialize, Deserialize)]
struct Message {
    name: String,
    text: Vec<String>,
    title: String,
    keybind: Option<String>,
    checkbox: Option<bool>,
}

#[derive(Debug, Serialize, Deserialize)]
struct Tab {
    title: String,
    sources: Vec<PathBuf>,
    with: Option<HashMap<String, String>>,
    app: Option<AppTabType>,
}

impl Tab {
    pub fn get_messages() {
    }

}

#[derive(Debug, Serialize, Deserialize)]
enum AppTabType {
    squadjoin,
}

fn load_bundles(config_path: PathBuf) -> Result<(), Box<std::error::Error>> {
    let conf = settings::Settings::new(&config_path)?;
    println!("{:?}", conf);

    let bundler_path = config_path
        .parent()
        .unwrap()
        .join(&conf.directories.bundlers);

    let mut bundles: Vec<Bundle> = Vec::new();

    for entry in WalkDir::new(&bundler_path)
        .into_iter()
        .filter_map(|e| e.ok())
    {
        if entry.file_type().is_file()
            && entry
                .file_name()
                .to_str()
                .map(|n| n.ends_with(".yml"))
                .unwrap_or(false)
        {
            let bundle = load_bundle(entry.path()).unwrap();
            bundles.push(bundle);
            println!("File: {}", entry.path().display());
        }
    }

    println!("{:?}", &bundles);

    Ok(())
}

fn load_bundle<P: Into<PathBuf>>(bundle_config_path: P) -> Result<Bundle, Box<std::error::Error>> {
    let f = std::fs::File::open(bundle_config_path.into())?;
    let b: Bundle = serde_yaml::from_reader(f)?;
    Ok(b)
}

fn load_source<P: Into<PathBuf>>(source_config_path: P, source_rela_path: String) -> Result<Source, Box<std::error::Error>> {
    let f = std::fs::File::open(source_config_path.into())?;
    let mut s: Source = serde_yaml::from_reader(f)?;
    s.rela_path = source_rela_path;
    Ok(s)
}

fn main() {
    let opt = Opt::from_args();
    load_bundles(opt.config_path).unwrap()
    //load_yaml(opt.bundler_root).unwrap();
}

/*
 * TODO: Cursive!
 * https://docs.rs/cursive/0/cursive/view/trait.Identifiable.html
 * as generator for unique ids for checkbox-button combo (or maybe checkboxes have click events ?)
 *
 * https://github.com/gyscos/Cursive/blob/master/doc/tutorial_3.md
 * describes identify->call things
 *
 * https://docs.rs/cursive/0.10.0/cursive/views/index.html
 * Buttons and checkboxes, views
 *
 *
 *
 *
 *
 */

